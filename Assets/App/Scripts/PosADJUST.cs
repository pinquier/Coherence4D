using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosADJUST : MonoBehaviour
{
    float x, y, z;

    public void AxeXplus()
      {
         x = transform.position.x;
         y = transform.position.y;
         z = transform.position.z;
        transform.position = new Vector3(x + .02f, y, z);
       
    }
      public void AxeXmoins()
      {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        transform.position = new Vector3(x - .02f, y, z);
    }
      public void AxeYplus()
      {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        transform.position = new Vector3(x , y + .02f, z);
      }
      public void AxeYmoins()
      {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        transform.position = new Vector3(x , y-0.02f, z);
    }
      public void AxeZplus()
      {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        transform.position = new Vector3(x, y, z + 0.02f);
    }
      public void AxeZmoins()
      {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        transform.position = new Vector3(x, y, z-0.02f);
    }

        
}
