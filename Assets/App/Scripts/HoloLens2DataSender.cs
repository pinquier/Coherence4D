using UnityEngine;
using System;
using System.Net.Sockets;
using System.Text;

public class HoloLens2DataSender : MonoBehaviour
{
    private string nodeRedIpAddress = "127.0.0.1";  //172.20.10.2
    private int nodeRedPort = 1880; 

    private TcpClient tcpClient;
    private NetworkStream stream;

    private void Start()
    {
        ConnectToNodeRed();
    }

    private void OnDestroy()
    {
        DisconnectFromNodeRed();
    }

    private void ConnectToNodeRed()
    {
        try
        {
            tcpClient = new TcpClient();
            tcpClient.Connect(nodeRedIpAddress, nodeRedPort);
            stream = tcpClient.GetStream();
            Debug.Log("Connected to Node-RED.");
        }
        catch (Exception e)
        {
            Debug.LogError("Error connecting to Node-RED: " + e.Message);
        }
    }

    private void DisconnectFromNodeRed()
    {
        if (tcpClient != null)
        {
            tcpClient.Close();
            Debug.Log("Disconnected from Node-RED.");
        }
    }

    public void OnMouseDown()
    {
        // Prepare your data to send as a JSON string (replace this with your actual data)
        string jsonData = "{\"key\": \"value\"}";

        // Send the data to Node-RED
        SendDataToNodeRed(jsonData);
    }

    private void SendDataToNodeRed(string data)
    {
        if (tcpClient == null || !tcpClient.Connected)
        {
            Debug.LogError("Not connected to Node-RED. Cannot send data.");
            return;
        }

        try
        {
            byte[] dataBytes = Encoding.UTF8.GetBytes(data);
            stream.Write(dataBytes, 0, dataBytes.Length);
            Debug.Log("Data sent to Node-RED successfully!");
        }
        catch (Exception e)
        {
            Debug.LogError("Error sending data to Node-RED: " + e.Message);
        }
    }
}
