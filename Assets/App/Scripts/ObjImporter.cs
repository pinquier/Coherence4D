using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class ObjImporter
{
    public static Mesh ImportFile(string filePath)
    {
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<int> triangles = new List<int>();
        List<Color> colors = new List<Color>(); // New list for storing colors

        string[] lines = File.ReadAllLines(filePath);

        foreach (string line in lines)
        {
            string[] parts = line.Split(' ');

            switch (parts[0])
            {
                case "v":
                    float x = float.Parse(parts[1]);
                    float y = float.Parse(parts[2]);
                    float z = float.Parse(parts[3]);
                    vertices.Add(new Vector3(x, y, z));
                    break;
                case "vt":
                    float u = float.Parse(parts[1]);
                    float v = float.Parse(parts[2]);
                    uvs.Add(new Vector2(u, v));
                    break;
                case "vn":
                    float nx = float.Parse(parts[1]);
                    float ny = float.Parse(parts[2]);
                    float nz = float.Parse(parts[3]);
                    normals.Add(new Vector3(nx, ny, nz));
                    break;
                case "f":
                    for (int i = 1; i < parts.Length; i++)
                    {
                        string[] indices = parts[i].Split('/');
                        int vertexIndex = int.Parse(indices[0]) - 1;
                        int uvIndex = int.Parse(indices[1]) - 1;
                        int normalIndex = int.Parse(indices[2]) - 1;
                        triangles.Add(vertexIndex);
                        triangles.Add(uvIndex);
                        triangles.Add(normalIndex);
                    }
                    break;
                case "vc": // Handle vertex color information
                    float r = float.Parse(parts[1]);
                    float g = float.Parse(parts[2]);
                    float b = float.Parse(parts[3]);
                    colors.Add(new Color(r, g, b));
                    break;
                // Handle other color-related statements, if present
                case "usemtl":
                    // Extract material color and assign it to vertices
                    break;
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.colors = colors.ToArray(); // Assign the colors to the mesh vertices
        mesh.RecalculateBounds();

        return mesh;
    }
}
