using UnityEngine;
using UnityEngine.XR.WSA;
using System.Collections;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using Microsoft.MixedReality.Toolkit;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;



public class Calibration : MonoBehaviour, IMixedRealitySpeechHandler
{
    private IMixedRealitySpatialAwarenessMeshObserver spatialObserver;
    private bool isSpatialMappingEnabled = false;
    public GameObject AnchoredObject, LoadedMesh, indexFingerObject, indexFingerObject2, indexFingerObject3, indexFingerObjectClone, indexFingerObjectClone2, indexFingerObjectClone3;
    private List<Vector3> touchedVertices = new List<Vector3>(); // List to store touched vertices
    MixedRealityPose pose;
    public Material[] wallMaterial = new Material[10];
    public Material floorMaterial;
    Vector3 PositionOfPlane, PositionOfPlane2, PositionOfPlane3, vOne, vTwo, vThree, vOnewall1, vTwowall1, vThreewall1, vOnewall2, vTwowall2, vThreewall2, SavedFingerPos, SavedFingerPos2, SavedFingerPos3;
    Vector3 vOneUpdate, vTwoUpdate, vThreeUpdate, vOneUpdatewall1, vTwoUpdatewall1, vThreeUpdatewall1, vOneUpdatewall2, vTwoUpdatewall2, vThreeUpdatewall2;
    bool floorupdate, wall1update, wall2update;
    bool UpdateAllignment = false;
    public GameObject ExportedAnchorPlans;
 




    private void Start()
    {
        spatialObserver = CoreServices.GetSpatialAwarenessSystemDataProvider<IMixedRealitySpatialAwarenessMeshObserver>();
        CoreServices.InputSystem.RegisterHandler<IMixedRealitySpeechHandler>(this);
        floorupdate = false; wall1update = false; wall2update = false;
    }

    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {

        if (eventData.Command.Keyword.Equals("FLOOR"))
        {
            floorupdate = false;
            Debug.Log("Floor detected");
            indexFingerObject.GetComponent<Renderer>().enabled = false;

            if (HandJointUtils.TryGetJointPose(TrackedHandJoint.IndexTip, Handedness.Right, out MixedRealityPose pose))
            {

                indexFingerObject.GetComponent<Renderer>().enabled = true;

                // Find the closest triangle to the index finger
                float closestDistance = float.MaxValue;
                int closestTriangleIndex = -1;

                foreach (var meshObject in spatialObserver.Meshes.Values)
                {
                    if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                    {

                        MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                        MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                        Mesh mesh = meshFilter.sharedMesh;

                        Vector3[] vertices = mesh.vertices;
                        int[] triangles = mesh.triangles;

                        Transform meshParentTransform = meshFilter.transform.parent;
                        Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                        for (int i = 0; i < triangles.Length; i += 3)
                        {
                            int vIndex1 = triangles[i];
                            int vIndex2 = triangles[i + 1];
                            int vIndex3 = triangles[i + 2];

                            Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                            Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                            Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                            Vector3 centroid = (v1 + v2 + v3) / 3f;
                            float distanceToIndexFinger = Vector3.Distance(centroid, pose.Position);
                            SavedFingerPos = pose.Position;
                            if (distanceToIndexFinger < closestDistance)
                            {
                                closestDistance = distanceToIndexFinger;
                                closestTriangleIndex = i;
                                PositionOfPlane = centroid;
                                vOne = v1; // to be used later out of the loop;
                                vTwo = v2;
                                vThree = v3;
                            }
                        }
                    }
                }

                if (closestTriangleIndex != -1)
                {
                    // Set the position
                    indexFingerObject.transform.position = PositionOfPlane;

                    Vector3 normal = Vector3.Cross(vTwo - vOne, vThree - vOne).normalized;
                    Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                    rotation *= Quaternion.Euler(90f, 0f, 0f);
                    indexFingerObject.transform.rotation = rotation;

                    Debug.Log("Floor Plane position: " + indexFingerObject.transform.position);
                    Debug.Log("Finger index position: " + pose.Position);
                    floorupdate = true;
                }
            }
        }

        if (eventData.Command.Keyword.Equals("One"))
        {
            wall1update = false;
            Debug.Log("Wall 1 detected");
            indexFingerObject2.GetComponent<Renderer>().enabled = false;

            if (HandJointUtils.TryGetJointPose(TrackedHandJoint.IndexTip, Handedness.Right, out MixedRealityPose pose))
            {

                indexFingerObject2.GetComponent<Renderer>().enabled = true;

                // Find the closest triangle to the index finger
                float closestDistance = float.MaxValue;
                int closestTriangleIndex = -1;

                foreach (var meshObject in spatialObserver.Meshes.Values)
                {
                    if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                    {

                        MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                        MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                        Mesh mesh = meshFilter.sharedMesh;

                        Vector3[] vertices = mesh.vertices;
                        int[] triangles = mesh.triangles;

                        Transform meshParentTransform = meshFilter.transform.parent;
                        Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                        for (int i = 0; i < triangles.Length; i += 3)
                        {
                            int vIndex1 = triangles[i];
                            int vIndex2 = triangles[i + 1];
                            int vIndex3 = triangles[i + 2];

                            Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                            Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                            Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                            Vector3 centroid = (v1 + v2 + v3) / 3f;
                            float distanceToIndexFinger = Vector3.Distance(centroid, pose.Position);
                            SavedFingerPos2 = pose.Position;
                            if (distanceToIndexFinger < closestDistance)
                            {
                                closestDistance = distanceToIndexFinger;
                                closestTriangleIndex = i;
                                PositionOfPlane2 = centroid;
                                vOnewall1 = v1; // to be used later out of the loop;
                                vTwowall1 = v2;
                                vThreewall1 = v3;
                            }
                        }
                    }
                }

                if (closestTriangleIndex != -1)
                {
                    // Set the position
                    indexFingerObject2.transform.position = PositionOfPlane2;

                    Vector3 normal = Vector3.Cross(vTwowall1 - vOnewall1, vThreewall1 - vOnewall1).normalized;
                    Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                    rotation *= Quaternion.Euler(90f, 0f, 0f);
                    indexFingerObject2.transform.rotation = rotation;

                    Debug.Log("Wall1 Plane position: " + indexFingerObject2.transform.position);
                    Debug.Log("Finger index position: " + pose.Position);
                    wall1update = true;
                }
            }
        }
        if (eventData.Command.Keyword.Equals("Two"))
        {
            wall2update = false;
            Debug.Log("Wall 2 detected");
            indexFingerObject3.GetComponent<Renderer>().enabled = false;

            if (HandJointUtils.TryGetJointPose(TrackedHandJoint.IndexTip, Handedness.Right, out MixedRealityPose pose))
            {

                indexFingerObject3.GetComponent<Renderer>().enabled = true;

                // Find the closest triangle to the index finger
                float closestDistance = float.MaxValue;
                int closestTriangleIndex = -1;

                foreach (var meshObject in spatialObserver.Meshes.Values)
                {
                    if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                    {

                        MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                        MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                        Mesh mesh = meshFilter.sharedMesh;

                        Vector3[] vertices = mesh.vertices;
                        int[] triangles = mesh.triangles;

                        Transform meshParentTransform = meshFilter.transform.parent;
                        Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                        for (int i = 0; i < triangles.Length; i += 3)
                        {
                            int vIndex1 = triangles[i];
                            int vIndex2 = triangles[i + 1];
                            int vIndex3 = triangles[i + 2];

                            Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                            Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                            Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                            Vector3 centroid = (v1 + v2 + v3) / 3f;
                            float distanceToIndexFinger = Vector3.Distance(centroid, pose.Position);
                            SavedFingerPos3 = pose.Position;
                            if (distanceToIndexFinger < closestDistance)
                            {
                                closestDistance = distanceToIndexFinger;
                                closestTriangleIndex = i;
                                PositionOfPlane3 = centroid;
                                vOnewall2 = v1; // to be used later out of the loop;
                                vTwowall2 = v2;
                                vThreewall2 = v3;
                            }
                        }
                    }
                }

                if (closestTriangleIndex != -1)
                {
                    // Set the position
                    indexFingerObject3.transform.position = PositionOfPlane3;

                    Vector3 normal = Vector3.Cross(vTwowall2 - vOnewall2, vThreewall2 - vOnewall2).normalized;
                    Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                    rotation *= Quaternion.Euler(90f, 0f, 0f);
                    indexFingerObject3.transform.rotation = rotation;

                    Debug.Log("Wall1 Plane 2 position: " + indexFingerObject3.transform.position);
                    Debug.Log("Finger index position: " + pose.Position);
                    wall2update = true;
                }
            }
        }
    }

    void Update()
    {
        if (UpdateAllignment)
        {
            // Align indexFingerObjectClone with indexFingerObject in the (x, z) plane
            AlignObjectsInPlane(indexFingerObjectClone.transform, indexFingerObject.transform, 1);

            // Align indexFingerObjectClone2 with indexFingerObject2 in the (y, x) plane
            AlignObjectsInPlane(indexFingerObjectClone2.transform, indexFingerObject2.transform, 2);

            // Align indexFingerObjectClone3 with indexFingerObject3 in the (y, z) plane
            AlignObjectsInPlane(indexFingerObjectClone3.transform, indexFingerObject3.transform, 3);
        }

        if (floorupdate)
        {

            // Find the closest triangle to the index finger
            float closestDistance = float.MaxValue;
            int closestTriangleIndex = -1;

            foreach (var meshObject in spatialObserver.Meshes.Values)
            {
                if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                {

                    MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                    MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                    Mesh mesh = meshFilter.sharedMesh;

                    Vector3[] vertices = mesh.vertices;
                    int[] triangles = mesh.triangles;

                    Transform meshParentTransform = meshFilter.transform.parent;
                    Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                    for (int i = 0; i < triangles.Length; i += 3)
                    {
                        int vIndex1 = triangles[i];
                        int vIndex2 = triangles[i + 1];
                        int vIndex3 = triangles[i + 2];

                        Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                        Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                        Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                        Vector3 centroid = (v1 + v2 + v3) / 3f;
                        float distanceToIndexFinger = Vector3.Distance(centroid, SavedFingerPos);
                        if (distanceToIndexFinger < closestDistance)
                        {
                            closestDistance = distanceToIndexFinger;
                            closestTriangleIndex = i;
                            PositionOfPlane = centroid;
                            vOneUpdate = v1; // to be used later out of the loop;
                            vTwoUpdate = v2;
                            vThreeUpdate = v3;
                        }
                    }
                }
            }

            if (closestTriangleIndex != -1)
            {
                // Set the position
                indexFingerObject.transform.position = PositionOfPlane;

                Vector3 normal = Vector3.Cross(vTwoUpdate - vOneUpdate, vThreeUpdate - vOneUpdate).normalized;
                Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                rotation *= Quaternion.Euler(90f, 0f, 0f);
                indexFingerObject.transform.rotation = rotation;

            }
        }
        if (wall1update)
        {

            // Find the closest triangle to the index finger
            float closestDistance = float.MaxValue;
            int closestTriangleIndex = -1;

            foreach (var meshObject in spatialObserver.Meshes.Values)
            {
                if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                {

                    MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                    MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                    Mesh mesh = meshFilter.sharedMesh;

                    Vector3[] vertices = mesh.vertices;
                    int[] triangles = mesh.triangles;

                    Transform meshParentTransform = meshFilter.transform.parent;
                    Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                    for (int i = 0; i < triangles.Length; i += 3)
                    {
                        int vIndex1 = triangles[i];
                        int vIndex2 = triangles[i + 1];
                        int vIndex3 = triangles[i + 2];

                        Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                        Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                        Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                        Vector3 centroid = (v1 + v2 + v3) / 3f;
                        float distanceToIndexFinger = Vector3.Distance(centroid, SavedFingerPos2);
                        if (distanceToIndexFinger < closestDistance)
                        {
                            closestDistance = distanceToIndexFinger;
                            closestTriangleIndex = i;
                            PositionOfPlane2 = centroid;
                            vOneUpdatewall1 = v1; // to be used later out of the loop;
                            vTwoUpdatewall1 = v2;
                            vThreeUpdatewall1 = v3;
                        }
                    }
                }
            }

            if (closestTriangleIndex != -1)
            {
                // Set the position
                indexFingerObject2.transform.position = PositionOfPlane2;

                Vector3 normal = Vector3.Cross(vTwoUpdatewall1 - vOneUpdatewall1, vThreeUpdatewall1 - vOneUpdatewall1).normalized;
                Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                rotation *= Quaternion.Euler(90f, 0f, 0f);
                indexFingerObject2.transform.rotation = rotation;

            }
        }
        if (wall2update)
        {

            // Find the closest triangle to the index finger
            float closestDistance = float.MaxValue;
            int closestTriangleIndex = -1;

            foreach (var meshObject in spatialObserver.Meshes.Values)
            {
                if (meshObject is SpatialAwarenessMeshObject spatialMeshObject)
                {

                    MeshFilter meshFilter = spatialMeshObject.GameObject.GetComponent<MeshFilter>();
                    MeshRenderer meshRenderer = spatialMeshObject.GameObject.GetComponent<MeshRenderer>();
                    Mesh mesh = meshFilter.sharedMesh;

                    Vector3[] vertices = mesh.vertices;
                    int[] triangles = mesh.triangles;

                    Transform meshParentTransform = meshFilter.transform.parent;
                    Matrix4x4 meshParentMatrix = meshParentTransform != null ? meshParentTransform.localToWorldMatrix : Matrix4x4.identity;

                    for (int i = 0; i < triangles.Length; i += 3)
                    {
                        int vIndex1 = triangles[i];
                        int vIndex2 = triangles[i + 1];
                        int vIndex3 = triangles[i + 2];

                        Vector3 v1 = meshParentMatrix.MultiplyPoint(vertices[vIndex1]);
                        Vector3 v2 = meshParentMatrix.MultiplyPoint(vertices[vIndex2]);
                        Vector3 v3 = meshParentMatrix.MultiplyPoint(vertices[vIndex3]);

                        Vector3 centroid = (v1 + v2 + v3) / 3f;
                        float distanceToIndexFinger = Vector3.Distance(centroid, SavedFingerPos3);
                        if (distanceToIndexFinger < closestDistance)
                        {
                            closestDistance = distanceToIndexFinger;
                            closestTriangleIndex = i;
                            PositionOfPlane3 = centroid;
                            vOneUpdatewall2 = v1; // to be used later out of the loop;
                            vTwoUpdatewall2 = v2;
                            vThreeUpdatewall2 = v3;
                        }
                    }
                }
            }

            if (closestTriangleIndex != -1)
            {
                // Set the position
                indexFingerObject3.transform.position = PositionOfPlane3;

                Vector3 normal = Vector3.Cross(vTwoUpdatewall2 - vOneUpdatewall2, vThreeUpdatewall2 - vOneUpdatewall2).normalized;
                Quaternion rotation = Quaternion.LookRotation(-normal, Vector3.up);
                rotation *= Quaternion.Euler(90f, 0f, 0f);
                indexFingerObject3.transform.rotation = rotation;

            }
        }

    }

    public void CreateOrthonormalFrame()
    {
        floorupdate = false;
        wall1update = false;
        wall2update = false;

        // Create a new GameObject to represent the orthonormal frame
        GameObject orthonormalFrame = new GameObject("OrthonormalFrame");

        // Set the position of the orthonormal frame as the average position of the given objects
        Vector3 averagePosition = (indexFingerObject.transform.position + indexFingerObject2.transform.position + indexFingerObject3.transform.position) / 3f;
        orthonormalFrame.transform.position = averagePosition;

        // Calculate the forward direction of the orthonormal frame as the normalized sum of the given object's forward directions
        Vector3 forwardDirection = (indexFingerObject.transform.forward + indexFingerObject2.transform.forward + indexFingerObject3.transform.forward).normalized;
        orthonormalFrame.transform.forward = forwardDirection;

        // Calculate the right and up directions of the orthonormal frame using cross products
        Vector3 rightDirection = Vector3.Cross(indexFingerObject.transform.forward, indexFingerObject2.transform.forward).normalized;
        Vector3 upDirection = Vector3.Cross(rightDirection, forwardDirection).normalized;

        // Set the right and up directions of the orthonormal frame
        orthonormalFrame.transform.right = rightDirection;
        orthonormalFrame.transform.up = upDirection;

        // Optional: Attach the orthonormal frame as a child of the current GameObject
        orthonormalFrame.transform.SetParent(transform);

        // Optional: Adjust the scale of the orthonormal frame if needed
        orthonormalFrame.transform.localScale = Vector3.one;

        // Debug the position of AnchoredObject relative to the orthonormal frame
        Vector3 anchoredObjectPositionRelativeToFrame = orthonormalFrame.transform.InverseTransformPoint(AnchoredObject.transform.position);
        Debug.Log("AnchoredObject Position Relative to Orthonormal Frame: " + anchoredObjectPositionRelativeToFrame);

        ExportAndSaveObjects();

    }

    public void ExportAndSaveObjects()
    {
        SaveObjectConstraints(indexFingerObject, "indexFingerObject");
        SaveObjectConstraints(indexFingerObject2, "indexFingerObject2");
        SaveObjectConstraints(indexFingerObject3, "indexFingerObject3");
        SaveObjectConstraints(AnchoredObject, "AnchoredObject");
    }

    private void SaveObjectConstraints(GameObject obj, string objName)
    {

        Vector3 positionDiff = obj.transform.position;
        PlayerPrefs.SetFloat(objName + "_constraint_x", positionDiff.x);
        PlayerPrefs.SetFloat(objName + "_constraint_y", positionDiff.y);
        PlayerPrefs.SetFloat(objName + "_constraint_z", positionDiff.z);


        // Save the normal vector constraint between this object and the next one

        Vector3 normal = obj.transform.up;
        PlayerPrefs.SetFloat(objName + "_constraint_normal_x", normal.x);
        PlayerPrefs.SetFloat(objName + "_constraint_normal_y", normal.y);
        PlayerPrefs.SetFloat(objName + "_constraint_normal_z", normal.z);

        Debug.Log("Constraints saved for " + objName);
    }

    public void ImportObjects()
    {
        ApplyObjectConstraints(indexFingerObjectClone, "indexFingerObject");
        ApplyObjectConstraints(indexFingerObjectClone2, "indexFingerObject2");
        ApplyObjectConstraints(indexFingerObjectClone3, "indexFingerObject3");
        ApplyObjectConstraints(AnchoredObject, "AnchoredObject");
    }

    private void ApplyObjectConstraints(GameObject obj, string objName)
    {
        Vector3 positionDiff = GetConstraint(objName);
        obj.transform.position = positionDiff;


        Vector3 normal = GetNormalConstraint(objName);
        obj.transform.up = normal;

        Debug.Log("Constraints applied for " + objName);
    }



    private Vector3 GetConstraint(string objName1)
    {
        float constraintX = PlayerPrefs.GetFloat(objName1 + "_constraint_x", 0f);
        float constraintY = PlayerPrefs.GetFloat(objName1 + "_constraint_y", 0f);
        float constraintZ = PlayerPrefs.GetFloat(objName1 + "_constraint_z", 0f);

        return new Vector3(constraintX, constraintY, constraintZ);
    }


    private Vector3 GetNormalConstraint(string objName1)
    {
        float normalX = PlayerPrefs.GetFloat(objName1 + "_constraint_normal_x", 0f);
        float normalY = PlayerPrefs.GetFloat(objName1 + "_constraint_normal_y", 1f);
        float normalZ = PlayerPrefs.GetFloat(objName1 + "_constraint_normal_z", 0f);

        return new Vector3(normalX, normalY, normalZ);
    }
    public void AllignReperes()
    {
        UpdateAllignment = true;
       
    }

    private void AlignObjectsInPlane(Transform objectToAlign, Transform referenceObject, int a)
    {
        Vector3 positionOffset = Vector3.zero;
        Quaternion rotationOffset = Quaternion.identity;

        if (a == 1)
        {
            positionOffset = new Vector3(0, referenceObject.position.y - objectToAlign.position.y, 0);
        }
        else if (a == 2)
        {
            positionOffset = new Vector3(0, 0, referenceObject.position.z - objectToAlign.position.z);
        }
        else if (a == 3)
        {
            positionOffset = new Vector3(referenceObject.position.x - objectToAlign.position.x, 0, 0);
        }

        // Apply the position offset to all clones
        indexFingerObjectClone.transform.position += positionOffset;
        indexFingerObjectClone2.transform.position += positionOffset;
        indexFingerObjectClone3.transform.position += positionOffset;
        AnchoredObject.transform.position += positionOffset;

        // Calculate the rotation difference between the objectToAlign and referenceObject
        Quaternion rotationDifference = Quaternion.FromToRotation(objectToAlign.forward, referenceObject.forward);

        // Apply the same rotation to all clones
        indexFingerObjectClone.transform.rotation = rotationDifference * indexFingerObjectClone.transform.rotation;
        indexFingerObjectClone2.transform.rotation = rotationDifference * indexFingerObjectClone2.transform.rotation;
        indexFingerObjectClone3.transform.rotation = rotationDifference * indexFingerObjectClone3.transform.rotation;
        AnchoredObject.transform.rotation = rotationDifference * AnchoredObject.transform.rotation;
    }



    /*public void SaveColoredSpatialMappingData()
    {
        var meshData = spatialObserver.Meshes;
        Debug.Log("Mesh data count: " + meshData.Count);

        if (meshData.Count > 0)
        {
            Debug.Log("Mesh data is not empty");

            int firstKey = meshData.Keys.First();
            SpatialAwarenessMeshObject firstMeshData;

            if (meshData.TryGetValue(firstKey, out firstMeshData))
            {
                Debug.Log("First mesh ID: " + firstMeshData.Id);

                var meshFilter = firstMeshData.GameObject.GetComponent<MeshFilter>();
                var meshRenderer = firstMeshData.GameObject.GetComponent<MeshRenderer>();

                if (meshFilter != null && meshRenderer != null)
                {
                    Debug.Log("Mesh filter and renderer are not null");

                    var mesh = meshFilter.sharedMesh;
                    var materials = meshRenderer.sharedMaterials;

                    if (mesh != null && materials != null)
                    {
                        Debug.Log("Mesh and materials are not null");

                        var filePath = Application.persistentDataPath + "/colored_spatial_mesh.obj";
                        Debug.Log("Exporting colored mesh to file: " + filePath);

                        // Create a new mesh with colors
                        Mesh coloredMesh = new Mesh();
                        coloredMesh.vertices = mesh.vertices;
                        coloredMesh.triangles = mesh.triangles;
                        coloredMesh.colors = new Color[mesh.vertices.Length];

                        // Assign colors to vertices based on materials
                        for (int i = 0; i < mesh.subMeshCount; i++)
                        {
                            var subMeshTriangles = mesh.GetTriangles(i);
                            var colorIndex = i % materials.Length;
                            var color = materials[colorIndex].color;

                            for (int j = 0; j < subMeshTriangles.Length; j++)
                            {
                                var vertexIndex = subMeshTriangles[j];
                                coloredMesh.colors[vertexIndex] = color;
                            }
                        }

                        // Save the colored mesh to file
                        ObjExporter.MeshToFile(coloredMesh, filePath);

                        Debug.Log("Colored spatial mapping mesh data saved to: " + filePath);

                        // Loop through the vertices and log their values
                        foreach (var vertex in coloredMesh.vertices)
                        {
                            Debug.Log("Vertex position: " + vertex);
                        }
                    }
                    else
                    {
                        Debug.Log("Mesh or materials are null");
                    }
                }
                else
                {
                    Debug.Log("Mesh filter or renderer is null");
                }
            }
            else
            {
                Debug.Log("First mesh data is not found in the dictionary");
            }
        }
    }


    public void LoadColoredSpatialMappingData()
    {
        var filePath = Application.persistentDataPath + "/colored_spatial_mesh.obj";
        Debug.Log("Loading colored mesh from file: " + filePath);

        if (File.Exists(filePath))
        {
            Debug.Log("File exists: " + filePath);

            var loadedMesh = ObjImporter.ImportFile(filePath);

            if (loadedMesh != null)
            {
                Debug.Log("Mesh loaded successfully");

                // Access the colors of the loaded mesh
                var loadedColors = loadedMesh.colors;

                // Debug the number of vertices and colors in the loaded mesh
                Debug.Log("Number of vertices in loaded mesh: " + loadedMesh.vertexCount);
                Debug.Log("Number of colors in loaded mesh: " + loadedColors.Length);

                // Do further processing with the loaded mesh and its colors as needed
            }
            else
            {
                Debug.Log("Failed to load mesh: " + filePath);
            }
        }
        else
        {
            Debug.Log("File does not exist: " + filePath);
        }
    }*/


    public void ToggleSpatialMappingVisibility()
        {
            if (spatialObserver != null)
            {
                isSpatialMappingEnabled = !isSpatialMappingEnabled;
                spatialObserver.DisplayOption = isSpatialMappingEnabled ? SpatialAwarenessMeshDisplayOptions.Visible : SpatialAwarenessMeshDisplayOptions.None;
            }
        }
    }
