using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draw3DObjects : MonoBehaviour
{
    Mesh _mesh2;
    public Vector3[] vertices = new Vector3[10000];
    public GameObject Basic_Drawing_Object1, HandFollow1, MeshFilter;
    public GameObject[] Alpha1 = new GameObject[10000];
    public GameObject[] Alpha2 = new GameObject[10000];
    int cpt = 0, cpt2=0;
    int surfacedrawer = 0;
    int linecounter = 0;
    public int tri = 0;
    public int k = 0;
    int duplications;
    float distance;
    float incrementation;
    public Transform x;
    public int[] triangles= new int[30000];
    public Material Green, Blue, Yellow, Grey;
    public List<Vector3> Ligne1 = new List<Vector3>();
    public List<Vector3> Ligne2 = new List<Vector3>();
    public List<Vector3> Ligne3 = new List<Vector3>();
    public List<Vector3> Ligne4 = new List<Vector3>();


    // function positionning vertices 
    // Fucntion contrainte geo, freez axis 
    // function that allows creating a line  
    // Second funtion that allows creating a surface based on many lines
    //Draw contours on the created surface
    // Extrusion and soustraction
    //force on hands that allow creating edge and wavy surfaces

    public void AllowDrawing() // change by vocal command
    {
        x = HandFollow1.transform;
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, x.position, x.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezX()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(Alpha1[cpt - 1].transform.position.x, x.position.y, x.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezY()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(x.position.x, Alpha1[cpt - 1].transform.position.y, x.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezZ()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(x.position.x, x.position.y, Alpha1[cpt - 1].transform.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezPaneXY()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(Alpha1[cpt - 1].transform.position.x, Alpha1[cpt - 1].transform.position.y, x.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezPaneXZ()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(Alpha1[cpt - 1].transform.position.x, x.position.y, Alpha1[cpt - 1].transform.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }
    public void FreezPaneYZ()
    {
        x = HandFollow1.transform;
        var vect = new Vector3(x.position.x, Alpha1[cpt - 1].transform.position.y, Alpha1[cpt - 1].transform.position.z);
        Alpha1[cpt] = Instantiate(Basic_Drawing_Object1, vect, x.transform.rotation);
        LineCreator();
        cpt++;
    }


    void LineCreator()
    {
        if (linecounter == 0)
        {
            Ligne1.Add(Alpha1[cpt].transform.position);
        }

        if (linecounter == 1)
        {
            Ligne2.Add(Alpha1[cpt].transform.position);
            Alpha1[cpt].GetComponent<MeshRenderer>().material = Green;
        }

        if (linecounter == 2)
        {
            Ligne3.Add(Alpha1[cpt].transform.position);
            Alpha1[cpt].GetComponent<MeshRenderer>().material = Blue;
        }

        if (linecounter == 3)
        {
            Ligne4.Add(Alpha1[cpt].transform.position);
            Alpha1[cpt].GetComponent<MeshRenderer>().material = Yellow;
        }
    }

    public void NextLine()
    {
        linecounter++;
        Debug.Log("Line1 count is" + Ligne1.Count);
        Debug.Log("Line2 count is" + Ligne2.Count);
    }

    public void SurfaceCreator()
    {
        surfacedrawer++;
        if (surfacedrawer == 1)
        {

        duplications = Ligne2.Count;// nombre de duplications de sommets de la ligne
        
        distance = Vector3.Distance(Ligne2[duplications-1] , Ligne2[0]) ;// longueur entre sommet 0 et n perpendiculaire � la ligne � dupliquer
        Debug.Log("Distance is  " + distance);
        incrementation = distance / duplications; // pas entre deux sommets
        Debug.Log("incrementation are " + incrementation);
        Vector3 vect = new(incrementation, 0f, 0f);
        
        for(int i=1; i<Ligne1.Count+1; i++)
        {
            for(int j=1; j < duplications+1; j++)
            {
                Alpha2[cpt2] = Instantiate(Basic_Drawing_Object1, Ligne1[i] + j * vect, x.transform.rotation);
                Alpha2[cpt2].GetComponent<MeshRenderer>().material = Grey;
                cpt2++;
            }
        }

        }
        if (surfacedrawer == 2)
        {
            MeshCreation();
        }

        if (surfacedrawer == 3)
        {
            triangleCreation();
           /* for(int i=0; i < cpt + 1; i++)
            {
                Alpha1[cpt].SetActive(false);
            }
            for (int i = 0; i < cpt + 1; i++)
            {
                Alpha2[cpt2].SetActive(false);
            }*/
        } 

    }

    void MeshCreation()
    {
        _mesh2 = MeshFilter.GetComponent<MeshFilter>().mesh;
        for(int i = 0; i < Ligne1.Count; i++)
        {
            vertices[i] = Ligne1[i]; //Line1 Vertices being stored in the vertices array
        }
        if (Ligne2.Count > 0)
        {
            for (int i = Ligne1.Count; i < Ligne1.Count + Ligne2.Count; i++)
            {
                vertices[i] = Ligne2[i-Ligne1.Count]; //Line2 Vertices being stored in the vertices array
            }
        }
      
        if (Ligne3.Count > 0)
        {
                 for (int i =Ligne1.Count+ Ligne2.Count; i < Ligne1.Count+ Ligne3.Count + Ligne2.Count; i++)
                 {
                     vertices[i] = Ligne3[i-(Ligne1.Count + Ligne2.Count)]; //Line3 Vertices being stored in the vertices array
                 }
        }

        if (Ligne4.Count > 0)
        {
            for (int i = Ligne1.Count + Ligne2.Count+Ligne3.Count; i < Ligne1.Count + Ligne2.Count+Ligne3.Count + Ligne4.Count; i++)
            {
                vertices[i] = Ligne4[i-(Ligne1.Count + Ligne2.Count + Ligne3.Count)]; //Line4 Vertices being stored in the vertices array
            }
        }

        for (int i = Ligne1.Count + Ligne2.Count + Ligne3.Count + Ligne4.Count; i < Ligne1.Count + Ligne2.Count + Ligne3.Count + Ligne4.Count + cpt2; i++)
        {
            vertices[i] = Alpha2[i-(Ligne1.Count + Ligne2.Count + Ligne3.Count + Ligne4.Count)].transform.position; //duplicated Vertices being stored in the vertices array
        }
        
    }

    void triangleCreation()
    {
        int alpha2vertices = Ligne1.Count + Ligne2.Count + Ligne3.Count + Ligne4.Count;
        //Hide all gameobject spheres

        // Function Ligne 1

        for (int o = 0; o < Ligne1.Count-2; o++)
        {

        triangles[tri] = k+1;
        triangles[tri+1] = k+2;
        triangles[tri+2] = alpha2vertices+ k*duplications;

        triangles[tri+3] = k + 2;
        triangles[tri + 4] = alpha2vertices + (k+1) * duplications;
        triangles[tri + 5] = alpha2vertices + k * duplications;

            k++;
            tri += 6;
        }
         for(int b = 0; b < Ligne2.Count-1; b++)
         {
             for(int a = 0; a < Ligne1.Count-2; a++)
             {
                triangles[tri] = alpha2vertices + a * duplications + b;
                triangles[tri+1] = alpha2vertices + (a+1) * duplications + b;
                triangles[tri+2] = alpha2vertices + a * duplications + b+1;

                triangles[tri+3] = alpha2vertices + (a + 1) * duplications + b;
                triangles[tri + 4] = alpha2vertices + (a+1) * duplications + b + 1;
                triangles[tri+5] = alpha2vertices + a * duplications + b + 1;
                k++;
                tri += 6;
            }
        }
        for (int o = 0; o < Ligne1.Count - 2; o++)
        {

            triangles[tri] = k + 1;
            triangles[tri + 1] = k + 2;
            triangles[tri + 2] = alpha2vertices + k * duplications;

            triangles[tri + 3] = k + 2;
            triangles[tri + 4] = alpha2vertices + (k + 1) * duplications;
            triangles[tri + 5] = alpha2vertices + k * duplications;

            k++;
            tri += 6;
        }
        _mesh2.Clear();
        _mesh2.vertices = vertices;
        _mesh2.triangles = triangles;
        _mesh2.RecalculateBounds();
        _mesh2.RecalculateNormals();
        MeshFilter.SetActive(true);
        MeshFilter.GetComponent<MeshFilter>().mesh = _mesh2;
       //MeshFilter.transform.position = Alpha1[0].transform.position;
       // MeshFilter.transform.rotation = Alpha1[0].transform.rotation;

    }
}
