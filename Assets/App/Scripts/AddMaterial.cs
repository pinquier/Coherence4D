using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMaterial : MonoBehaviour
{

    Mesh _mesh2;
    public GameObject Basic_Drawing_Object, HandFollow, MeshFilter, ButtonStartDrawing, ButtonEndDrawing, ReferencePlan;
    GameObject Surface2, Alpha;
    public Vector3[] Cubes=new Vector3[10000];
    public Vector3[] vertices = new Vector3[10000];
    Vector3 replacement;
    public int[] triangles;
    bool Condition = false, EndDrawing = false;
    float step = 0.1f;
    int vertices_number;
    int real_count = 10000;
    int cpt = -1, button_cond = 0;
    float max_Value_x = -1000f, max_Value_z = -1000f;
    float min_Value_x = 100f, min_Value_z = 100f;
    private void Start()
    {
        _mesh2 = MeshFilter.GetComponent<MeshFilter>().mesh; // r�cup�rer le mesh du gameobject (cube)
        ButtonEndDrawing.SetActive(false);
    }
    void Update()
    {
        var x = HandFollow.transform.position; // position de la bille
        if (cpt == 0 && EndDrawing == false)
        {
            Alpha = Instantiate(Basic_Drawing_Object, x, HandFollow.transform.rotation);
            Alpha.transform.SetParent(ReferencePlan.transform);// added 07/12
            
            cpt++;
            
            Cubes[0] = transform.InverseTransformPoint(Alpha.transform.position);
        }

        else if (cpt > 0 && x != Alpha.transform.position && EndDrawing == false) // If I've moved my hand from previous position
        {

            Alpha = Instantiate(Basic_Drawing_Object, x, HandFollow.transform.rotation);
            Alpha.transform.SetParent(ReferencePlan.transform);// added 07/12
            if (Alpha.transform.position.x != 0 || Alpha.transform.position.z != 0)
            {
                Cubes[cpt] = transform.InverseTransformPoint(Alpha.transform.position);
                cpt++;
            }
            ModifyShape();
        }
    }

    private void OnDrawGizmos()
    {
        if (vertices == null && !Condition)
            return;
        for (int i = 0; i < vertices.Length; i++)
        {

            Gizmos.DrawSphere(vertices[i], .03f);

        }
    }
    void ModifyShape()
    {
        if (!Condition)
        {
            vertices = new Vector3[10000];
            int v = 0;
            for (int i = 0, j = 1, z = 0; z <= cpt; z++)
            {
                if (Cubes[i] != null && Cubes[j] != null) // z->y, y>-z
                {
                    vertices[0] = Cubes[0];
                    vertices[0].y = 0;
                    if (Mathf.Abs(Cubes[i].x - Cubes[j].x) > step || Mathf.Abs(Cubes[i].z - Cubes[j].z) > step)
                    {

                        vertices[v] = Cubes[j]; // create a vertex for each instantiated cube
                        vertices[v].y = 0;
                        i = j;
                        j++;
                        v++;

                    }
                    else
                    {
                        j++;
                    }
                }
            }
        }
    }

    void UpdateMesh()
    {
        triangles = new int[30000];
        int tris = 0;
        int b = 521;

        bool ok1 = false, ok2 = false;

        for (int x = 0; x < 1000; x++)
        {
            while (tris != 0 && (vertices[x].x == 0 && vertices[x].z == 0 && x < 2 * real_count))
            {
                x++;
            }
            if (tris == 0 || vertices[x].x != 0 || vertices[x].z != 0)
            {
                triangles[tris] = x;
                ok1 = true;
            }
            while ((ok1 == true && vertices[x + 1].x == 0 && vertices[x + 1].z == 0) && x < 2 * real_count)
            {
                x++;
            }
            if (vertices[x + 1].x != 0 || vertices[x + 1].z != 0)
            {
                triangles[tris + 1] = x + 1;
                ok2 = true;
            }
            while (ok2 == true && vertices[521].x == 0 && vertices[521].z == 0)
            {
                b += 1;
            }
            if (vertices[b].x != 0 || vertices[b].z != 0)
            {
                triangles[tris + 2] = b;
            }

            tris += 3;
        }

        _mesh2.Clear();
        _mesh2.vertices = vertices;
        _mesh2.triangles = triangles;
        _mesh2.RecalculateBounds();

        _mesh2.RecalculateNormals();

        MeshFilter.GetComponent<MeshFilter>().mesh = _mesh2;
        Condition = true;
    }


    public void EndDrawing2D() //buton
    {
        for (int m = 0; m < vertices.Length; m++)
        {
            if (vertices[m].x == 0 && vertices[m].y == 0 && vertices[m].z == 0)
            {
                real_count--;
            }
        }
        EndDrawing = true;
        SelectCenter();
    }

    public void StartDrawing()
    {
        cpt = 0;
        ButtonStartDrawing.SetActive(false);
        ButtonEndDrawing.SetActive(true);
    }
    private void SelectCenter()
    {

        for (int mo = 0; mo < real_count + 1; mo++)
        {
            if (vertices[mo].x > max_Value_x) { max_Value_x = vertices[mo].x; }
            if (vertices[mo].z > max_Value_z) { max_Value_z = vertices[mo].z; }
            if (vertices[mo].x < min_Value_x) { min_Value_x = vertices[mo].x; }
            if (vertices[mo].z < min_Value_z) { min_Value_z = vertices[mo].z; }

        }
        vertices[521].x = (max_Value_x + min_Value_x) / 2;
        vertices[521].z = (max_Value_z + min_Value_z) / 2;
        vertices[521].y = 0;

        UpdateMesh();
        MeshFilter.transform.position = new Vector3(0, 0, 0);
        //Extrusion();

        Instantiate(MeshFilter, new Vector3(MeshFilter.transform.position.x, MeshFilter.transform.position.y + 3f, MeshFilter.transform.position.x), Quaternion.identity);

        LateralSurface();
    }

   /* void Extrusion()
    {
        // create points in y parallel
        for (int p = 0; p < 5 * real_count; p++)
        {
            vertices[p + 1000].x = vertices[p].x;
            vertices[p + 1000].y = vertices[p].y;
            vertices[p + 1000].z = vertices[p].z;
        }
        _mesh2.vertices = vertices;

    }*/

    void LateralSurface()
    {
        //1- GET New object Mesh
        //2- Replace vertices with the following ones : traingle 1 = point 1 in countour 1 + point 2 in contoure 1 + point 1 in contoure 2
        //triangle 2 = point point 2 in contour 1 + point 2 in contoure 2 + point 1 in contour 1 
    }
}

