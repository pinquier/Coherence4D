using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButonsManagement : MonoBehaviour
{
    public GameObject Panel1, ActivePanel1, HidePanel1;
    public void ShowVerticesDrawingButons()
    {
        Panel1.SetActive(true);
        ActivePanel1.SetActive(false);
        HidePanel1.SetActive(true);
    }
    public void HideVerticesDrawingButons()
    {
        Panel1.SetActive(false);
        ActivePanel1.SetActive(true);
        HidePanel1.SetActive(false);
    }
}
