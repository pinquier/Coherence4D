using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;

public class ChunkController : MonoBehaviour
{
    private IMixedRealityDataProviderAccess dataProviderAccess;
    private IMixedRealitySpatialAwarenessMeshObserver spatialMappingObserver;

    private void Start()
    {
        dataProviderAccess = CoreServices.SpatialAwarenessSystem as IMixedRealityDataProviderAccess;

        if (dataProviderAccess != null)
        {
            spatialMappingObserver = dataProviderAccess.GetDataProvider<IMixedRealitySpatialAwarenessMeshObserver>();
        }
    }

    public void EnableChunk(int chunkIndex)
    {
        if (spatialMappingObserver != null && chunkIndex >= 0 && chunkIndex < spatialMappingObserver.Meshes.Count)
        {
            spatialMappingObserver.Meshes[chunkIndex].GameObject.SetActive(true);
        }
    }

    public void DisableChunksExceptFirst()
    {
        if (spatialMappingObserver != null && spatialMappingObserver.Meshes.Count > 0)
        {
            for (int i = 1; i < spatialMappingObserver.Meshes.Count; i++)
            {
                spatialMappingObserver.Meshes[i].GameObject.SetActive(false);
            }
        }
    }
}
