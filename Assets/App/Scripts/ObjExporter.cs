using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class ObjExporter
{
    public static void MeshToFile(Mesh mesh, string filePath)
    {
        using (StreamWriter sw = new StreamWriter(filePath))
        {
            // Write file header or comments
            sw.WriteLine("# OBJ file generated by Unity " + Application.unityVersion);
            sw.WriteLine("# Exported on: " + System.DateTime.Now.ToString());

            // Export vertices
            foreach (Vector3 vertex in mesh.vertices)
            {
                sw.WriteLine("v " + vertex.x + " " + vertex.y + " " + vertex.z);
            }

            // Export UVs if present
            if (mesh.uv.Length > 0)
            {
                foreach (Vector2 uv in mesh.uv)
                {
                    sw.WriteLine("vt " + uv.x + " " + uv.y);
                }
            }

            // Export normals if present
            if (mesh.normals.Length > 0)
            {
                foreach (Vector3 normal in mesh.normals)
                {
                    sw.WriteLine("vn " + normal.x + " " + normal.y + " " + normal.z);
                }
            }

            // Export colors if present
            Color[] colors = mesh.colors;
            if (colors.Length > 0)
            {
                foreach (Color color in colors)
                {
                    sw.WriteLine("vc " + color.r + " " + color.g + " " + color.b);
                }
            }

            // Export triangles
            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                int index0 = mesh.triangles[i] + 1;
                int index1 = mesh.triangles[i + 1] + 1;
                int index2 = mesh.triangles[i + 2] + 1;
                sw.WriteLine("f " + index0 + "/" + index0 + "/" + index0 + " " +
                    index1 + "/" + index1 + "/" + index1 + " " +
                    index2 + "/" + index2 + "/" + index2);
            }
        }
    }
}
